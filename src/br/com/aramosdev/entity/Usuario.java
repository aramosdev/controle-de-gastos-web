package br.com.aramosdev.entity;

import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Table;

@Table(name="Usuarios")
public class Usuario {
	@EntityDb(field="id_usuario")
	private int id_usuario;
	@EntityDb(field="nome")	
	private String nome;
	@EntityDb(field="login")
	private String login;
	@EntityDb(field="email")
	private String email;
	@EntityDb(field="senha")
	private String senha;
	
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
}
