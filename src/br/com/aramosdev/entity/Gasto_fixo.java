package br.com.aramosdev.entity;

import java.math.BigDecimal;
import java.sql.Date;

import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Table;


@Table(name="Gastos_fixos")
public class Gasto_fixo {
	@EntityDb(field="id_gasto_fixo")
	private int id_gasto_fixo;
	@EntityDb(field="nome_gasto")
	private String nome;
	@EntityDb(field="descricao_gasto")
	private String descricao;
	@EntityDb(field="date")
	private Date date;
	@EntityDb(field="valor")
	private BigDecimal valor;
	
	public int getId_gasto_fixo() {
		return id_gasto_fixo;
	}
	public void setId_gasto_fixo(int id_gasto_fixo) {
		this.id_gasto_fixo = id_gasto_fixo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
