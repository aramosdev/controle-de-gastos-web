package br.com.aramosdev.entity;

import java.math.BigDecimal;

import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Table;

@Table(name="produtos")
public class Produto {
	@EntityDb(field="id_produto")
	private int id_produto;
	@EntityDb(field="id_gasto_dia",entity="Gasto_dia")
	private Gasto_dia gasto_dia;
	@EntityDb(field="id_categoria",entity="Categoria")
	private Categoria categoria;
	@EntityDb(field="nome")
	private String nome;
	@EntityDb(field="descricao")
	private String descricao;
	@EntityDb(field="valor")
	private BigDecimal valor;
	@EntityDb(field="local")
	private String local;

	public int getId_produto() {
		return id_produto;
	}
	public void setId_produto(int id_produto) {
		this.id_produto = id_produto;
	}
	public Gasto_dia getGasto_dia() {
		return gasto_dia;
	}
	public void setGasto_dia(Gasto_dia gasto_dia) {
		this.gasto_dia = gasto_dia;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
}
