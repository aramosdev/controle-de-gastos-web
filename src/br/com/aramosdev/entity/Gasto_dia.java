package br.com.aramosdev.entity;

import java.math.BigDecimal;
import java.sql.Date;

import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Id;
import br.com.aramosdev.annotations.Table;

@Table(name="gastos_dia")
public class Gasto_dia {
	@Id
	@EntityDb(field="id_gasto_dia")
	private int id_gasto_dia;
	@EntityDb(field="id_categoria",entity="Categoria")
	private Categoria categoria;
	@EntityDb(field="id_usuario",entity="Usuario")
	private Usuario usuario;
	@EntityDb(field="nome_gasto")
	private String nome;
	@EntityDb(field="descricao_gasto")
	private String descricao;
	@EntityDb(field="date")
	private Date date;
	@EntityDb(field="valor")
	private BigDecimal valor;
	
	public int getId_gasto_dia() {
		return id_gasto_dia;
	}
	public void setId_gasto_dia(int id_gasto_dia) {
		this.id_gasto_dia = id_gasto_dia;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
