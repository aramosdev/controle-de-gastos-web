package br.com.aramosdev.entity;

import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Id;
import br.com.aramosdev.annotations.Table;

@Table(name="categorias")
public class Categoria {
	@Id
	@EntityDb(field="id_categoria")
	private int id_categoria;
	@EntityDb(field="nome_categoria")
	private String nome_categoria;
	@EntityDb(field="descricao_categoria")
	private String descricao_categoria;
	@EntityDb(field="status")
	private boolean status;
	
	public int getId_categoria() {
		return id_categoria;
	}
	public void setId_categoria(int id_categoria) {
		this.id_categoria = id_categoria;
	}
	public String getNome_categoria() {
		return nome_categoria;
	}
	public void setNome_categoria(String nome_categoria) {
		this.nome_categoria = nome_categoria;
	}
	public String getDescricao_categoria() {
		return descricao_categoria;
	}
	public void setDescricao_categoria(String descricao_categoria) {
		this.descricao_categoria = descricao_categoria;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}
}
