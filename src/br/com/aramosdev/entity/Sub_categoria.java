package br.com.aramosdev.entity;

import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Table;

@Table(name="sub_categorias")
public class Sub_categoria {
	@EntityDb(field="id_sub_categoria")
	private int id_sub_categoria;
	@EntityDb(field="id_categoria",entity="Categoria")
	private Categoria categoria;
	@EntityDb(field="nome")
	private String nome;
	@EntityDb(field="descricao")
	private String descricao;
	
	public int getId_sub_categoria() {
		return id_sub_categoria;
	}
	public void setId_sub_categoria(int id_sub_categoria) {
		this.id_sub_categoria = id_sub_categoria;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
}
