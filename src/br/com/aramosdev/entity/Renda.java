package br.com.aramosdev.entity;

import java.math.BigDecimal;
import java.sql.Date;

import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Table;

@Table(name="rendas")
public class Renda {
	@EntityDb(field="id_renda")
	private int id_renda;
	@EntityDb(field="id_usuario",entity="Usuario")
	private Usuario usuario;
	@EntityDb(field="nome")
	private String nome;
	@EntityDb(field="descricao")
	private String descricao;
	@EntityDb(field="date")
	private Date date;
	@EntityDb(field="valor")
	private BigDecimal valor;
	
	public int getId_renda() {
		return id_renda;
	}
	public void setId_renda(int id_renda) {
		this.id_renda = id_renda;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
