package br.com.aramosdev.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Id;
import br.com.aramosdev.annotations.Table;

public class SqlEntityManager {
	private SqlHelpers sqlHelpers;
	private StringBuffer sql = new StringBuffer();
	private Object obj;

	public SqlEntityManager() {
		super();
		if(this.sqlHelpers == null){
			sqlHelpers = new SqlHelpers();
		}
	}
	public void getTable(Object obj) {
		this.sqlHelpers.setTable(obj.getClass().getAnnotation(Table.class).name());
	}
	public void createSqlInsert(Object obj) {
		this.sqlHelpers.getSql().append(" ");
		this.sqlHelpers.getSql().append("insert into ").append(this.sqlHelpers.getTable()).append(" (");
		this.sql.append(" values(");
		try {
			Field[] fields = obj.getClass().getDeclaredFields();
			String[] fieldsEntitys = new String[fields.length -1];
			int i = 0;
			for (Field field : fields) {
				if(!field.isAnnotationPresent(Id.class)){
					EntityDb anotacao = field.getAnnotation(EntityDb.class);
					this.sqlHelpers.getSql().append(anotacao.field()).append(",");
					this.sql.append(" ? ,");
					fieldsEntitys[i] = field.getName();
					i++;
				}
			}
			this.sqlHelpers.setFields(fieldsEntitys);
			this.sqlHelpers.getSql().deleteCharAt(this.sqlHelpers.getSql().length() - 1);
			this.sql.deleteCharAt(this.sql.length() - 1);
			this.sql.append(")");
			this.sqlHelpers.getSql().append(")").append(this.sql);
		} catch (Exception e) {
			e.getMessage();
		}
	}
	public void getValuesInsertDb(Object obj) throws Exception {
		Class<? extends Object> cls = obj.getClass();
		BeanInfo info = Introspector.getBeanInfo(cls, Object.class);
		PropertyDescriptor[] props = info.getPropertyDescriptors();
		Object[] values = new Object[this.sqlHelpers.getFields().length];
		for (int j = 0; j < values.length; j++) {
			for (int i = 0; i < props.length; i++) {
				PropertyDescriptor pd = props[i];
				String name = pd.getName();
				System.out.println(this.sqlHelpers.getFields()[j] +" = "+name );
				if (this.sqlHelpers.getFields()[j].equals(name)) {
					Field field = obj.getClass().getDeclaredField(name);
					EntityDb entity = field.getAnnotation(EntityDb.class);
					Method getter = pd.getReadMethod();
					values[j] = (Object) getter.invoke(obj, null);
					if (!field.isAnnotationPresent(Id.class)) {
						if (!entity.entity().isEmpty()) {
							if (values[j] == null) {
								values[j] = values[j];
							} else {
								values[j] = this.getValueId(values[j]);
							}
						}
					}else if(field.isAnnotationPresent(Id.class) || values[j] != null){
						values[j] = (Object) getter.invoke(obj, null);
					}
				}
			}
		}
		this.sqlHelpers.setValues(values);
	}
	public SqlHelpers getSqlInsert(Object obj) throws Exception {
		this.obj = obj;
		this.getTable(obj);
		this.createSqlInsert(obj);
		this.getValuesInsertDb(obj);
		return this.sqlHelpers;
	}
	public void createSqlDelete(Object obj){
		StringBuffer sql = new StringBuffer();
		sql.append(" ");
		sql.append("delete from ").append(this.sqlHelpers.getTable());
		sql.append(" where ");
		Field[] fields = obj.getClass().getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].isAnnotationPresent(Id.class)) {
				sql.append(fields[i].getAnnotation(EntityDb.class).field()).append("=?");
				break;
			}
		}
		this.sqlHelpers.setSql(sql);
	}
	public void createSqlUpdate(Object obj){
		StringBuffer sql = new StringBuffer();
		StringBuffer where = new StringBuffer();
		sql.append("update ").append(this.sqlHelpers.getTable());
		sql.append(" set ");
		Field[] fields = obj.getClass().getDeclaredFields();
		String[] fieldsEntitys = new String[fields.length];
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].isAnnotationPresent(EntityDb.class) && !fields[i].isAnnotationPresent(Id.class)){
				fieldsEntitys[i -1] = fields[i].getName();
				sql.append(fields[i].getAnnotation(EntityDb.class).field()).append("=?, ");
			}else if(fields[i].isAnnotationPresent(Id.class)){
				fieldsEntitys[fieldsEntitys.length-1] = fields[i].getName();
				where.append(" where ").append(fields[i].getAnnotation(EntityDb.class).field()).append("=?");				
			}
		}
		sql.deleteCharAt(sql.length() -2);
		sql.append(where);
		this.sqlHelpers.setFields(fieldsEntitys);
		this.sqlHelpers.setSql(sql);
	}
	public Object getValueId(Object obj) throws Exception{
		Class<? extends Object> clsEntity = obj.getClass();
		Field[] fields = clsEntity.getDeclaredFields();
		for (int i = 0; i < fields.length; i++) {
			if (fields[i].isAnnotationPresent(Id.class)) {
				Method m = clsEntity.getMethod("get"+ Text.firstUpperCase(fields[i].getName()), null);
				return m.invoke(obj, null);
			}
		}
		return null;
	}
	public SqlHelpers getSqlDelete(Object obj) throws Exception{
		this.getTable(obj);
		this.createSqlDelete(obj);
		Object[] values = new Object[1];
		values[0] = getValueId(obj);
		this.sqlHelpers.setValues(values);
		return this.sqlHelpers;		
	}
	public SqlHelpers getSqlUpdate(Object obj) throws Exception{
		this.getTable(obj);
		this.createSqlUpdate(obj);
		this.getValuesInsertDb(obj);
		return this.sqlHelpers;		
	}
	
}