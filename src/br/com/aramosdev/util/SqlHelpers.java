package br.com.aramosdev.util;

public class SqlHelpers {
	private StringBuffer sql;
	private String table;
	private String[] fields;
	private Object[] values;
	
	public SqlHelpers() {
		this.sql =  new StringBuffer();
	}
	public StringBuffer getSql() {
		return sql;
	}
	public void setSql(StringBuffer sql) {
		this.sql = sql;
	}
	public String[] getFields() {
		return fields;
	}
	public void setFields(String[] fields) {
		this.fields = fields;
	}
	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public Object[] getValues() {
		return values;
	}
	public void setValues(Object[] values) {
		this.values = values;
	}
}
