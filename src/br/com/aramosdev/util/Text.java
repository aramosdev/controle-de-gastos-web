package br.com.aramosdev.util;

public class Text {
	public static String firstUpperCase(String text) {
        final StringBuilder result = new StringBuilder(text);
        int i = 0;
        do {
            result.replace(i, i + 1, result.substring(i,i + 1).toUpperCase());
            i =  result.indexOf(" ", i) + 1;
        } while (i > 0 && i < result.length());
        return result.toString();
    }
}
