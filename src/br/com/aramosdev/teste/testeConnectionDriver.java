package br.com.aramosdev.teste;

import java.math.BigDecimal;

import br.com.aramosdev.entity.Categoria;
import br.com.aramosdev.entity.Gasto_dia;
import br.com.aramosdev.jdbc.EntityManager;

public class testeConnectionDriver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManager manager = new EntityManager();
		Gasto_dia gasto_dia  = new Gasto_dia();
		
		Categoria cat  = new Categoria();
		cat.setId_categoria(2);
		cat.setNome_categoria("Alimentašao");
		cat.setDescricao_categoria("Gastos de alimentos");
		cat.setStatus(true);
		gasto_dia.setCategoria(cat);
		gasto_dia.setNome("Beto");
		gasto_dia.setDescricao("Vamos");
		BigDecimal valor = new BigDecimal(02.10);
		gasto_dia.setValor(valor);
		gasto_dia.setId_gasto_dia(3);
		manager.updateEntity(gasto_dia);
	}

}
