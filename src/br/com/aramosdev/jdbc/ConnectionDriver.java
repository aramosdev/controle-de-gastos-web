package br.com.aramosdev.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import br.com.aramosdev.annotations.ConfigDatabase;

public class ConnectionDriver {
	
 	public static Connection getConnection() {
		Connection conn;
		String url = ConfigDatabase.url+ConfigDatabase.database;
		String user = ConfigDatabase.login;
		String password = ConfigDatabase.password;
		try {
			conn = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			conn = null;
			System.err.print(e.getMessage());
		}
		return conn;
	}
}
