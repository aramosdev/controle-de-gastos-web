package br.com.aramosdev.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;

import br.com.aramosdev.annotations.EntityDb;
import br.com.aramosdev.annotations.Id;
import br.com.aramosdev.annotations.Table;
import br.com.aramosdev.util.SqlEntityManager;
import br.com.aramosdev.util.SqlHelpers;
import br.com.aramosdev.util.Text;

public class EntityManager {
	private Connection conn = ConnectionDriver.getConnection();
	private Object obj;
	private SqlEntityManager sqlEntityManager;
	
	public EntityManager() {
		super();
		if(this.sqlEntityManager == null){
			sqlEntityManager= new SqlEntityManager();
		}
	}

	public boolean saveEntity(Object obj) {
		boolean resultDb = false;
		this.obj = obj;
		try {
			SqlHelpers sqlHelper = this.sqlEntityManager.getSqlInsert(obj);
			Object[] valuesInsert = sqlHelper.getValues();
			PreparedStatement preStatement = null;
			preStatement = this.conn.prepareStatement(sqlHelper.getSql().toString());
			System.out.println(preStatement);
			for (int i = 0; i < valuesInsert.length; i++) {
				Object object = valuesInsert[i];
				preStatement.setObject(i + 1, object);
			}
			resultDb = preStatement.execute();
			preStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultDb;
	}
	public boolean deleteEntity(Object obj) {
		boolean resultDb = false;
		this.obj = obj;
		try {
			SqlHelpers sqlHelper = this.sqlEntityManager.getSqlDelete(obj);
			Object[] valuesInsert = sqlHelper.getValues();
			PreparedStatement preStatement = null;
			preStatement = this.conn.prepareStatement(sqlHelper.getSql().toString());
			for (int i = 0; i < valuesInsert.length; i++) {
				Object object = valuesInsert[i];
				preStatement.setObject(i + 1, object);
			}
			resultDb = preStatement.execute();
			preStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultDb;
	}
	public boolean updateEntity(Object obj) {
		boolean resultDb = false;
		this.obj = obj;
		try {
			SqlHelpers sqlHelper = this.sqlEntityManager.getSqlUpdate(obj);
			Object[] valuesInsert = sqlHelper.getValues();
			PreparedStatement preStatement = null;
			preStatement = this.conn.prepareStatement(sqlHelper.getSql().toString());
			for (int i = 0; i < valuesInsert.length; i++) {
				Object object = valuesInsert[i];
				preStatement.setObject(i + 1, object);
			}
			System.out.println("\n"+preStatement);
			resultDb = preStatement.execute();
			preStatement.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultDb;
	}
}
 