package br.com.aramosdev.annotations;

public @interface ConfigDatabase {

	static String url = "jdbc:postgresql://localhost:5432/";
	static String database = "db_controle_gastos";
	static String login = "postgres";
	static String password = "root";
}
